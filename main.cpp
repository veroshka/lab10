﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include "rectangle.hpp"
#include <iostream>
#include <vector>

using namespace std::chrono_literals;

int main()
{
    const int width = 800;
    const int height = 600;
    const int N = 10;

    sf::RenderWindow window(sf::VideoMode(width, height), L"Летающие прямоугольники!");

    std::vector <veroshka::Rectangle*> rectangles;

    for (int i = 0; i < width; i += width / N)
    {
        veroshka::Rectangle* rectangle = new veroshka::Rectangle(i, 0, 50, 22);
        rectangles.push_back(rectangle);
    }

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();

        for (const auto& rectangle : rectangles)
            window.draw(*rectangle->Get());

        for (const auto& rectangle : rectangles)
            rectangle->Move();


        window.display();

        std::this_thread::sleep_for(1ms);
    }

    for (const auto& rectangle : rectangles)
        delete rectangle;


    rectangles.clear();

    return 0;
}