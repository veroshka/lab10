#pragma once 
#include <SFML/Graphics.hpp>

namespace veroshka
{
	class Rectangle
	{
	public:
		Rectangle(int x, int y, float a, float b);

		~Rectangle();

		sf::RectangleShape* Get();

		void Move();

	private:
		int n_x, n_y;
		float n_a, n_b;
		sf::RectangleShape* n_shape;

	};
}