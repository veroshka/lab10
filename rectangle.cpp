#include "rectangle.hpp"

namespace veroshka
{
	Rectangle::Rectangle(int x, int y, float a, float b)
	{
		n_x = x;
		n_y = y;
		n_a = a;
		n_b = b;
		n_shape = new sf::RectangleShape(sf::Vector2f(n_a, n_b));
		n_shape->setOrigin(n_a, n_b);
		n_shape->setFillColor(sf::Color::Yellow);
		n_shape->setPosition(n_x, n_y);
	}


	Rectangle::~Rectangle()
	{
		delete n_shape;
	}

	sf::RectangleShape* Rectangle::Get() { return n_shape; }

	void Rectangle::Move()
	{
		n_y += 1;
		if (n_y <= 600)
		n_shape->setPosition(n_x, n_y);

	}

}